import React, { ReactElement } from 'react';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Page404, ProductsPage } from './pages';
import { rootStore } from './store/root/root';

const App = (): ReactElement => {
  return (
    <Provider store={rootStore}>
      <Switch>
        <Route path="/404">
          <Page404 />
        </Route>
        <Route path="/">
          <ProductsPage />
        </Route>
      </Switch>
      <ToastContainer />
    </Provider>
  );
};

export default App;
