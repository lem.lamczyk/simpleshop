import Product, { IProduct } from './product';

export type { IProduct };
export default Product;
