export default interface IProduct {
  readonly id: string;
  readonly name: string;
  readonly category: string;
  readonly imageUrl: string;
  readonly price: number;
}
