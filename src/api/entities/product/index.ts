import IProduct from './iProduct';
import Product from './product';

export type { IProduct };
export default Product;
