import { ProductResponse } from '../../responses';
import IProduct from './iProduct';

export default class Product implements IProduct {
  id: string;

  name: string;

  category: string;

  imageUrl: string;

  price: number;

  constructor(data: ProductResponse) {
    this.id = data.productId.value;
    this.name = data.name;
    this.category = data.category;
    this.price = data.price;
    this.imageUrl = data.imageUrl;
  }
}
