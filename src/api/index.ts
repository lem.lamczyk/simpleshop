import type { Client, ProductRepository } from './network';
import { AxiosClient, ProductRepositoryImpl } from './network';

import { ProductService } from './services';
import ProductServiceImpl from './services';

const client: Client = new AxiosClient();
const productRepository: ProductRepository = new ProductRepositoryImpl(client);
const productService: ProductService = new ProductServiceImpl(
  productRepository
);

export { productService };
