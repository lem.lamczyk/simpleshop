import axios, { AxiosInstance, AxiosResponse } from 'axios';
import Client from '../client';

export default class AxiosClient implements Client {
  httpClient: AxiosInstance;

  constructor() {
    this.httpClient = axios.create();
  }

  async get<T>(path: string): Promise<T> {
    const { httpClient } = this;
    return new Promise((resolve, reject) => {
      httpClient
        .get(path)
        .then((response: AxiosResponse) => resolve(response.data as T))
        .catch((error) => reject(error));
    });
  }
}
