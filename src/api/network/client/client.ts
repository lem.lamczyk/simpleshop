export default interface Client {
  get<T>(path: string): Promise<T>;
}
