import { Client } from "./client";
import AxiosClient from "./client";

import { ProductRepository } from "./product";
import ProductRepositoryImpl from "./product";

export type { Client, ProductRepository };
export { AxiosClient, ProductRepositoryImpl };