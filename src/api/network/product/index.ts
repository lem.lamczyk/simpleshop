import ProductRepository from './productRepository';
import ProductRepositoryImpl from './productRepositoryImpl';

export type { ProductRepository };
export default ProductRepositoryImpl;
