import { IProduct } from '../../entities';

export default interface ProductRepository {
  getProducts(): Promise<IProduct[]>;
}
