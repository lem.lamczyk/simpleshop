import type { Client } from '../client';
import Product, { IProduct } from '../../entities';
import ProductRepository from './productRepository';
import { ProductResponse } from '../../responses';

export default class ProductRepositoryImpl implements ProductRepository {
  client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  async getProducts(): Promise<IProduct[]> {
    const { client } = this;

    return new Promise((resolve, reject) => {
      client
        .get<ProductResponse[]>('mocks/products.json')
        .then((response: ProductResponse[]) => {
          const products = response.map(
            (productReponse) => new Product(productReponse)
          );
          resolve(products);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
