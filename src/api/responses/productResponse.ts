export type ProductResponse = {
  productId: {
    value: string;
  };
  name: string;
  price: number;
  imageUrl: string;
  category: string;
};
