import { ProductService } from "./product";
import ProductServiceImpl from "./product";

export type { ProductService };
export default ProductServiceImpl;

