import ProductService from './productService';
import ProductServiceImpl from './productServiceImpl';

export type { ProductService };
export default ProductServiceImpl;
