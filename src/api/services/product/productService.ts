import { IProduct } from '../../entities';

export default interface ProductService {
  getProducts(): Promise<IProduct[]>;
}
