import { IProduct } from '../../entities';
import { ProductRepository } from '../../network/product';
import ProductService from './productService';

export default class ProductServiceImpl implements ProductService {
  productRepository: ProductRepository;

  constructor(productRepository: ProductRepository) {
    this.productRepository = productRepository;
  }

  async getProducts(): Promise<IProduct[]> {
    const { productRepository } = this;
    return productRepository.getProducts();
  }
}
