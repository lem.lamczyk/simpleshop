import React, { ReactElement } from 'react';
import { Chip } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { useAppDispatch, useAppSelector } from '../../store/root/hooks';
import { selectCategory } from '../../store/products/slice';

import useStyles from './style';

const Categories = (): ReactElement => {
  const classes = useStyles();

  const dispatch = useAppDispatch();
  const { categories } = useAppSelector(({ products }) => products);

  const handleCategorySelection = (category: string | null) => {
    dispatch(selectCategory(category));
  };

  return (
    <div className={classes.root}>
      {categories.map((category, index) => (
        <Chip
          className={classes.chip}
          key={`${category}-${index}`}
          label={category}
          variant="outlined"
          onClick={() => handleCategorySelection(category)}
        />
      ))}
      <Chip
        className={classes.chip}
        label="Reset"
        variant="outlined"
        deleteIcon={<DeleteIcon />}
        onDelete={() => handleCategorySelection(null)}
        onClick={() => handleCategorySelection(null)}
      />
    </div>
  );
};

export default Categories;
