import React, { ReactElement } from 'react';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core';
import { useAppDispatch } from '../../store/root/hooks';
import { addToCart, removeFromCart } from '../../store/products/slice';
import useStyles from './style';

type Props = {
  id: string;
  name: string;
  imageUrl: string;
  numberInCart: number;
};

const ProductCard = (props: Props): ReactElement => {
  const { id, name, imageUrl, numberInCart } = props;
  const classes = useStyles();

  const dispatch = useAppDispatch();

  const handleAddToCart = () => {
    dispatch(addToCart(id));
  };

  const handleRemoveFromCart = () => {
    dispatch(removeFromCart(id));
  };

  return (
    <Card className={classes.card}>
      <CardMedia
        className={classes.media}
        component="img"
        src={imageUrl}
        title={name}
      />
      <CardContent className={classes.content}>
        <Typography className={classes.typography} variant="body2">
          {name}
        </Typography>
        <Typography className={classes.typography} variant="body2">
          {`In Cart: ${numberInCart}`}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions}>
        <Button
          size="small"
          variant="contained"
          onClick={handleAddToCart}
        >
          Add to cart
        </Button>
        <Button
          size="small"
          variant="outlined"
          color="secondary"
          disabled={!numberInCart}
          onClick={handleRemoveFromCart}
        >
          Remove from cart
        </Button>
      </CardActions>
    </Card>
  );
};

export default ProductCard;
