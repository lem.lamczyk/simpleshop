import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  card: {
    height: 300,
    width: 300,
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
  },
  media: {
    height: 154,
    objectFit: 'contain',
  },
  typography: {
    textAlign: 'center',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
  },
  actions: {
    padding: 0,
    justifyContent: 'center',
    alignContent: 'center',
  },
}));

export default useStyles;
