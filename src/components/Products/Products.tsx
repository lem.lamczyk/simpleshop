import React, { ReactElement } from 'react';
import { Grid } from '@material-ui/core';
import ProductCard from '../ProductCard';
import { useAppSelector } from '../../store/root/hooks';
import { ProductInfo } from '../../store/products/types';

const Products = (): ReactElement => {
  const { products, productIds, selectedCategory } = useAppSelector(
    ({ products }) => products
  );

  const filterProducts = (): ProductInfo[] => {
    if (!selectedCategory) return productIds.map((id: string) => products[id]);

    return productIds
      .map((id) => products[id])
      .filter((product) => product.category === selectedCategory);
  };

  return (
    <Grid container spacing={2}>
      {filterProducts().map((product) => (
        <Grid item key={product.id} xs={3}>
          <ProductCard
            id={product.id}
            name={product.name}
            imageUrl={product.imageUrl}
            numberInCart={product.numberInCart}
          />
        </Grid>
      ))}
    </Grid>
  );
};

export default Products;
