import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import {
  AppBar,
  Badge,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useAppSelector } from '../../store/root/hooks';
import useStyles from './style';

const TopBar = (): ReactElement => {
  const classes = useStyles();

  const { products, productIds } = useAppSelector(({ products }) => products);

  const numberOfUniqueProductsInCart = (): number => {
    let productsInCart = 0;
    productIds.forEach((id: string) => {
      if (products[id].numberInCart > 0) productsInCart += 1;
    });
    return productsInCart;
  };

  return (
    <AppBar className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Typography variant="h6" color="primary">
          Simple Shop
        </Typography>
        <IconButton component={Link} to="/404" color="inherit" edge="end">
          <Badge
            badgeContent={numberOfUniqueProductsInCart()}
            color="secondary"
          >
            <ShoppingCartIcon color="primary" />
          </Badge>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;
