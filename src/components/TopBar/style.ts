import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  appBar: {
    position: 'sticky',
    backgroundColor: 'white',
  },
  toolbar: {
    justifyContent: 'space-between',
  },
}));

export default useStyles;
