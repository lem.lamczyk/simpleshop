import React, { ReactElement } from "react";

const Page404 = (): ReactElement => <h1>404: Page not found</h1>

export default Page404;