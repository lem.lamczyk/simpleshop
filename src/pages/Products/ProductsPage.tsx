import React, { useEffect, ReactElement } from 'react';
import { Categories, TopBar, Products } from '../../components';
import { getProducts } from '../../store/products/thunks';
import { useAppDispatch } from '../../store/root/hooks';

const ProductsPage = (): ReactElement => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  return (
    <>
      <TopBar />
      <Categories />
      <Products />
    </>
  );
};

export default ProductsPage;
