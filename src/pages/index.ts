import ProductsPage from './Products';
import Page404 from './Page404';

export { ProductsPage, Page404 };
