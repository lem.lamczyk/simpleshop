import { ProductsState } from './types';

export const initialProductsState: ProductsState = {
  products: {},
  productIds: [],
  categories: [],
  selectedCategory: null,
};
