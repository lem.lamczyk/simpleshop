import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { initialProductsState } from './initialState';
import { getProducts } from './thunks';
import { ProductsState } from './types';
import { toast } from 'react-toastify';

export const productsSlice = createSlice({
  name: 'products',
  initialState: initialProductsState,
  reducers: {
    addToCart: (state: ProductsState, action: PayloadAction<string>) => {
      const productId = action.payload;
      state.products[productId].numberInCart += 1;
    },
    removeFromCart: (state: ProductsState, action: PayloadAction<string>) => {
      const productId = action.payload;
      const numberInCart = state.products[productId].numberInCart;
      state.products[productId].numberInCart = Math.max(0, numberInCart - 1);
    },
    selectCategory: (
      state: ProductsState,
      action: PayloadAction<string | null>
    ) => {
      state.selectedCategory = action.payload;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(getProducts.fulfilled, (state, action) => {
      const products = action.payload;
      const categories = products
        .map((product) => product.category)
        .filter((category) => category);

      state.products = products.reduce(
        (obj, cur) => ({ ...obj, [cur.id]: { ...cur, numberInCart: 0 } }),
        {}
      );
      state.productIds = products.map((product) => product.id);
      state.categories = [...new Set(categories)];
    });

    builder.addCase(getProducts.rejected, () => {
      toast.error('Error fetching products');
    });
  },
});

export const { addToCart, removeFromCart, selectCategory } =
  productsSlice.actions;

export const productsReducer = productsSlice.reducer;
