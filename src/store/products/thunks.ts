import { createAsyncThunk } from '@reduxjs/toolkit';
import { productService } from '../../api';

export const getProducts = createAsyncThunk(
  'products/getProducts',
  async () => {
    const products = await productService.getProducts();
    return products;
  }
);
