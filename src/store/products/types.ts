import { IProduct } from '../../api/entities';

export interface ProductInfo extends IProduct {
  numberInCart: number;
}

export interface ProductsState {
  products: Record<string, ProductInfo>;
  productIds: string[];
  categories: string[];
  selectedCategory: string | null;
}
