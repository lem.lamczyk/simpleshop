import { configureStore } from '@reduxjs/toolkit';
import { productsReducer } from '../products/slice';

export const rootStore = configureStore({
  reducer: {
    products: productsReducer,
  },
});
